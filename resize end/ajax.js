$.get('/test.txt', function(data) {
    console.log(data);
}).done(data => {
    $('body').append(data);
});

$.get('/test.json', function(data) {
    const container = $('body');
    Object.keys(data).forEach(key => {
        container.append(data[key]);
    });
});
