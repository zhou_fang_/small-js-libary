
// http://jsfiddle.net/04fLy8t4/
const initResizeEndEvent = (() => {
    let isResizing = false,
        timeOut;
    let interval = 200;
    let resizeStart = new Event('resizeStart');
    let resizeEnd = new Event('resizeEnd');
    let startResize = domElement => {
        isResizing = true;
        domElement.dispatchEvent(resizeStart, true);
        timeOut = setTimeout(() => {
            fireResizeEnd(domElement);
        }, interval);
    };

    let onResizing = domElement => {
        clearTimeout(timeOut);
        timeOut = setTimeout(() => {
            fireResizeEnd(domElement);
        }, interval);
    };

    let fireResizeEnd = domElement => {
        console.log('fire');
        domElement.dispatchEvent(resizeEnd, true);
        isResizing = false;
    };
    let blindToDom = (domElement, interval) => {
        domElement.addEventListener('resize', () => {
            console.log(isResizing);

            if (isResizing === true) {
                onResizing(domElement);
                console.log(isResizing);
            } else {
                startResize(domElement);
                console.log(isResizing);
            }
        });
    };

    return blindToDom;
})();

/* initResizeEndEvent(window);
window.addEventListener('resizeStart', () => {
    console.log('start')
});
window.addEventListener('resizeEnd', () => {
    console.log('end')
}); */

const ResizeEnd = class {
    constructor(domElement, interval) {
        this.domElement = domElement;
        this.interval = interval;
        this.isScrolling = false;
        this.timeOut;
        this.status = false;
        this.resizeStart = new Event('resizeStart');
        this.resizeEnd = new Event('resizeEnd');
        this.listener = () => {
            if (this.isScrolling) {
                console.log(this.isScrolling);
                this.onResizing(this.domElement);
            } else {
                this.triggerStart(this.domElement);
            }
        }
    }
    triggerStart(domElement) {
        this.isScrolling = true;
        domElement.dispatchEvent(this.resizeStart, true);
        this.timeOut = setTimeout(() => {
            this.triggerEnd(domElement);
        }, this.interval);
    }
    onResizing(domElement) {
        clearTimeout(this.timeOut);
        this.timeOut = setTimeout(() => {
            this.triggerEnd(domElement);
        }, this.interval);
    }

    triggerEnd(domElement) {
        this.isScrolling = false;
        domElement.dispatchEvent(this.resizeEnd, true);
    }
    init() {
        if (this.status === true) {
            return;
        }
        let _this = this;
        this.domElement.addEventListener('resize',
            this.listener
        );
        this.status = true;
    }
    cancel() {
        if (this.status === false) {
            return;
        }
        this.domElement.removeEventListener('resize', this.listener);

    }
};

let resizeEnd = new ResizeEnd(window, 1000);

//resizeEnd.init();

console.log(resizeEnd);
let showText = text => {
    let p = document.createElement('p');
    p.innerHTML = text;
    document.body.insertAdjacentElement('afterbegin', p);
};
window.addEventListener('resizeStart', () => {
    console.log('start');
    showText("start");
});
window.addEventListener('resizeEnd', () => {
    console.log('end');
    showText("end");
});