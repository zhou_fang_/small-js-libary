//array find the biggest n number

const sortN = (array, n) => {
    // if n > array.length, throw error
    // must be number array
    let temp = new Array(n).fill(null);
    //   temp[0] = array[0];
    const replaceTemp = (item, _temp) => {
        for (let i = 0; i < n; i++) {

            if (item >= temp[i]) {
                _temp.splice(i, 0, item);
                _temp.pop();
                break;
            }
        };
    };

    for (let i = 1; i < array.length; i++) {
        replaceTemp(array[i], temp);
    };
    return temp;
}




const testArray = [5, 4, 5, 6, 7, 5, 4, 76, 8];

const result = sortN(testArray, 3);
console.log(result);