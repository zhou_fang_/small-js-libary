//array find the biggest n number

const sortN1 = (array, n) => {
    const length = array.length;
    
    let temp: any = new Array(n).fill(null);
    const replaceTemp = item => {
        for (let i = 0; i < n; i++) {
            if (item > temp[i]) {
                temp.splice(i, 0, item);
                temp.pop();
                break;
            }
        }
    };

    for (let i = 1; i < array.length(); i++) {
        replaceTemp(array[i]);
    }
};
